# Importar biblioteca
# Utilizando la biblioteca pandas 
# Calcular la mediana, la media de los datos almacenada en una lista de 100 elementos
# la lista debe "llenarse" con numeros aleatorios de -100 a 100

import pandas as pd
from random import *

lista=[]

for i in range(1,101):
    n=randrange(-100,100)
    lista.append(n)
    
print(lista)
df=pd.DataFrame(lista,columns=['Valores Aleatorios'])
print(df)
mediana=df.median(axis=0)
print("Mediana de los",mediana)
media=df.mean(axis=0)
print("Media de los",media)
